---
title: Insec
subtitle: The Legendary Kick
player: Choi "Insec" In-Seok
playerInfo: Choi In-Seok started his professional eSports career in Korea playing for CJ Entus and KT Rolster Bullets. At his prime he was considered by many professional players to be the best jungler in the world. He later went on to play for the Chinese Royal Club organization and continued his professional career in the LPL.
year: 1234
quote: Q's in, wards back, kicks in. That is so hard to execute.
quoteAuthor: Jatt Leesman, Riot Associate Game Analyst
videos: {
    https://www.youtube.com/embed/T9jeFqgd5pw?autoplay=0&showinfo=0&rel=0&enablejsapi=1: Season 3 All Stars moment when inSec performed his signature technique against EU LCS winning the team fight. ..Credit to MadMarePro.,
    https://www.youtube.com/embed/JTLVhQCCRc0?autoplay=0&showinfo=0&rel=0&enablejsapi=1: A montage of professional play moments featuring variations of The Insec with ward jumps and flashes. ..Credit to iFunzio.,
    }
nextPage:
    url: /madlife
    name: Madlife
theme: 0
type: player
---

The Season 3 All Stars Tournament was one of the biggest international League of Legends events which drew the attention of a global audience. During a crucial match between EU and OGN All Stars, *Choi "Insec" In-Seok* single handedly won a teamfight by perfectly executing *The Insec*.

*The Insec* is a signature *Lee Sin* technique popularized by *Choi "Insec" In-Seok* after the memorable execution on the world stage. This technique is performed by ward jumping behind the target during a *Resonating Strike* and kicking the target towards your team with *Dragon's Rage*. *The Insec* became a staple technique for every *Lee Sin* player for its stylish and impactful execution. *Choi "Insec" In-Seok* became famous for his *Lee Sin* kick and was considered the best jungler in the world at the time due to his incredibly good mechanics and innovation.
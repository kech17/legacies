---
title: Soaz Escapes
subtitle: The Escape Artist
player: Paul "sOAZ" Boyer
playerInfo: Paul Boyer is a veteran professional player that started his career in 2010. Since then he has competed in both AllStar Tournament events and has qualified for 4 World Championships. His career in EULCS continues with his involvement with Origen and Fnatic.
year: 1234
quote: OH MY GOD! sOAZ is a GOD.
quoteAuthor: David "Phreak" Turley, NALCS Caster/analyst
videos: {
    https://www.youtube.com/embed/ZkDTFZ8kh14?autoplay=0&showinfo=0&rel=0&enablejsapi=1: sOAZ showcases his techniques on how to escape from certain death against other players on the professional stage. ..Credit to GAMEcut.,
    }
nextPage:
    url: /xpeke
    name: The xPeke
theme: 0
type: player
---

Against the World Championship team SKT T1 in the semifinals, *Paul "sOAZ" Boyer* displays his adaptability and slipperiness when it comes to top lane ganks. He manages to safely escape against two World Champions by using his *Teleport* ability at the perfect time. This was possible because SKT T1 did not have vision in the brush giving time for *sOAZ* to finish his channel.

This captured moment on the world stage featured his game knowledge, ingenuity, and mechanics to successfully outplay and escape a sure death. He continued to pull off his great escapes on the professional stage multiple times and is regarded as a slippery top laner that is hard to catch.
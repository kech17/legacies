---
title: Faker's Zed
subtitle: The Impossible 1v1
player: Lee "Faker" Sang-hyeok
playerInfo: Regarded as the best player in League of Legends history, Lee Sang-hyeok has secured 3 World Championship titles under his belt. He started his professional career when he was only 17 years old and has only ever played for SKT T1 as a mid laner. Many other professional players regard him as the best player currently in the scene.
year: 1234
quote: FAKER WHAT WAS THAT? Faker with a huuuuge play!
quoteAuthor: DoA and MonteCristo, Former OGN Casters
videos: {
    https://www.youtube.com/embed/ZPCfoCVCx3U?autoplay=0&showinfo=0&rel=0&enablejsapi=1: The famous game 5 blind pick between SKT T1 and KT Bullets where Faker beat Ryu in a Zed 1v1. ..Credit to MrSwayMedia,
    https://www.youtube.com/embed/YFCpCajop9k?autoplay=0&showinfo=0&rel=0&enablejsapi=1: Riot's Worlds feature on the historical rivalry between Ryu and Faker. ..Credit to LoL Esports,
    }
nextPage:
    url: /flamehorizon
    name: Flame Horizon
theme: 0
type: player
---

After four intense and close games, SKT T1 and KT Bullets play the fifth game of the OGN HOT6 Champions Final series. It is tradition for game 5 to feature blind pick to ensure fairness and balance for pick and ban orders for both teams. At the time, both *Ryu* and *Faker* were known for their mechanical prowess especially on assassin champions. Coincidentally, both players had blind picked *Zed*, arguably the most stylish assassin in the game.

This phenomenal play features *Faker's Zed* pushing at low health and getting caught out of position by *Ryu's Zed*. *Faker* beautifully outplays *Ryu* and wins the 1v1 fight with a jaw-dropping display of his mechanics. This moment in League of Legends history showed the world *Faker's* ability to outplay others and his masterful skills on *Zed*.
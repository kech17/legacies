---
title: Cpt Jack Cleanse
subtitle: The Frame Perfect Skill
player: Kang "Cpt Jack" Hyung-woo
playerInfo: Kang Hyung-woo is an AD Carry who played for many Korean teams during his professional eSports career. He was most recently a starter for Longzhu Gaming in 2016 but later pursued a career in casting and analytics at SPOTV.
year: 1234
quote: Look at this Cleanse. Beauuuuuutiful. Wow.
quoteAuthor: DoA and MonteCristo, Previous OGN Casters
videos: {
    https://www.youtube.com/embed/3CVAAeXcpgg?autoplay=0&showinfo=0&rel=0&enablejsapi=1: OGN HOT6 quarter finals playoffs where Kang "Cpt Jack" Hyung-woo pulls off a frame perfect cleanse from a Twisted Fate stun. ..Credit to JiYung,
    https://www.youtube.com/embed/x1rjjEOpusY?autoplay=0&showinfo=0&rel=0&enablejsapi=1: A montage of professional players successfully executing the Cpt Jack Cleanse negating a variety of hard crowd control effects. ..Credit to Fizz Khalifa,
    }
nextPage: 
    url: /dyrus
    name: Dyrus Flash
theme: 0
type: player
---

*Kang "Cpt Jack" Hyung-woo* is a former professional LCK AD Carry player known for his exceptional plays on *Ashe* and *Vayne*. He was awarded the Best AD Carry Award at the 2012 Korean eSports Awards for his mechanics and ability to carry games to victory. 

The *Cpt Jack Cleanse* is performed by perfectly timing a *Cleanse* to negate *crowd control* effects. If done correctly, it will seem as if no *crowd control* effects were ever applied to the champion.

All professional players strive to master this technique, however, it's extremely challenging since it requires sufficiently low ping and a vast knowledge on projectile timings. As difficult as it is, it is hugely useful as the frame perfect cleanse can ultimately determine the outcome of a game.
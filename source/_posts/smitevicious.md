---
title: Smitevicious
subtitle: The Smite God
player: Brandon "Saintvicious" DiMarco
playerInfo: Bradon DiMarco was an active professional player in NALCS until the 2015 season. He is regarded as a veteran player and has since transitioned into a coaching role for Team Dignitas. He was selected to participate in the 2013 AllStar's Tournament and is known for popularizing unconventional junglers and his unpredictive playstyle.
year: 1234
quote:
quoteAuthor:
videos: {
    https://www.youtube.com/embed/rRiJc3t68Fk?autoplay=0&showinfo=0&rel=0&enablejsapi=1: A montage of failed smites by Saintvicious over the course of his active professional eSports career. ..Credit to Troyd.,
    https://www.youtube.com/embed/YmMHfGIp3FI?autoplay=0&showinfo=0&rel=0&enablejsapi=1: An isolated clip from Dyrus's point of view when Saintvicious misses a smite on Baron Nashor. ..Credit to GG TV.,
    }
nextPage:
    url: /soaz
    name: sOAZ Escape
theme: 0
type: player
---

*Brandon "Saintvicious" DiMarco* is a veteran professional player known for his infamous moments of missing smite objectives at the most crucial times. He has missed so many smites on the professional stage that whenever a player does the same they are immediately referred to as *Saintvicious*.

*Smitevicious* originated from this running gag and is derived from combining the words "smite" and "saintvicious". This joke was so popular that a website was dedicated to keep track of the last time he missed a smite. Throughout the years, *Smitevicious* has evolved into a legacy left behind by *Brandon DiMarco* and exists to remind players the importance of smiting objectives.

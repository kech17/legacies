---
title: {{ title }}
subtitle: {{ subtitle }}
player: {{ player }}
playerInfo: {{ playerInfo }}
year: {{ year }}
quote: {{ quote }}
quoteAuthor: {{ quoteAuthor }}
videos: {{ videos }}
nextPage: {{ nextPage }}
theme: {{ theme }}
type: {{ type }}
---
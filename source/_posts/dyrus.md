---
title: Dyrus Flash
subtitle: The Great Escape
player: Marcus "Dyrus" Hill
playerInfo: Marcus Hill was the famous top laner on Team SoloMid and is considered a veteran due to his long career and experiences. He announced his retirement in 2015 after the World Championships but remains with the TSM brand as a streamer. However, along with other famous ex professional players, he joined the Stream Dream Meme Team (Delta Fox) in NACS for 2017.
year: 1234
quote: Uhh I was lagging.
quoteAuthor: Every Player that failed the Flash
videos: {
    https://www.youtube.com/embed/IBa0X-53wKk?autoplay=0&showinfo=0&rel=0&enablejsapi=1: Marcus "Dyrus" Hill flashes the top lane tribrush wall to escape while enemies fail their flashes. ..Credit to XynergyLoL,
    https://www.youtube.com/embed/I1zCxzLV4AQ?autoplay=0&showinfo=0&rel=0&enablejsapi=1: Trick2G (a well known high elo streamer) fails the Dyrus flash chasing an enemy on his best champion Udyr. ..Credit to xJohnishere,
    }
nextPage:
    url: /eggnivia
    name: Eggnivia Teleport
theme: 0
type: player
---

The *Dyrus Flash* is named after *Marcus "Dyrus" Hill* for being able to *Flash* over a notoriously thick wall on the topside of *Summoner's Rift*. It is not obvious that flashing this wall is possible as the *Flash* distance was changed a couple of times throughout its history. 

This *Flash* became famous after *Dyrus* was able to escape countless times in games because enemies failed to *Flash* over the wall during a chase. To this day, even professional players seem to fail this *Flash* and have a hard time mastering the technique. 

The secret to this technique is to position the champion at a sweet spot close to the wall and *Flash* over without accidentally pathing to the other side. You can actually perform this technique from either side of the wall!
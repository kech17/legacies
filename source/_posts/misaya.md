---
title: Misaya
subtitle: The Golden Bait
player: Yu "Misaya" Jingxi
playerInfo: Yu Jingxi was regarded as the best Twisted Fate player at the time and is remembered as one of the greatest mid laners of all time. His professional career allowed him to represent China at the Riot World Invitational and show the world the level of play the region had to offer. He unfortunately retired as a professional player in 2013.
year: 1234
quote:
quoteAuthor:
videos: {
    https://www.youtube.com/embed/h9mfCK1HEjw?autoplay=0&showinfo=0&rel=0&enablejsapi=1: The effectiveness of The Misaya demonstrated on the professional stage baiting out enemy team skills. ..Credit to LeoB,
    https://www.youtube.com/embed/ELxbAR_cM8Y?autoplay=0&showinfo=0&rel=0&enablejsapi=1: A variation of The Misaya where Zhonyas is used to cancel Twisted Fate ultimate. ..Credit to Mihkel Voodla,
    }
nextPage: 
    url: /pushforthewin
    name: Push For The Win
theme: 0
type: player
---
*Yu "Misaya" Jingxi* is the former mid laner and captain of the Chinese powerhouse Team WE. *The Misaya* is an unconventional play invented by him for his favorite champion, *Twisted Fate*. 

This technique involves using the *Zhonya's Hourglass* immediately after *teleporting* into the enemy team with *Destiny* and stunning an enemy with *Gold Card*. 

The play is counter intuitive at first but ultimately baits crucial enemy skills to be wasted and allowing your own team to catch up to the initiation. However, enemies may time their *crowd control* spells to interrupt the initiation so timing is key to a successful execution. 

*The Misaya* has now become a crucial technique to master for all avid *Twisted Fate* players!
---
title: 3k ELO Shockwave
subtitle: The Turnaround Play
player: Carl "ForellenLord" Lückmann
playerInfo: Carl Lückmann is the previous mid laner for Team Alternate and was the first player to ever reach 3000 elo in solo queue. In fact, he was also the first to reach any elo above 2700 in the game and thus was nicknamed "The Elo Lord". Carl was an active professional player from 2010 and ultimately retired at the end of 2013 to pursue personal endeavors. 
year: 1234
quote: The Shockwave catches ALL 5 OF THEM! Beautifully done!
quoteAuthor: Leigh "Deman" Smith, eSports Gaming Commentator
videos: {
    https://www.youtube.com/embed/r5cqOpF4DRw?autoplay=0&showinfo=0&rel=0&enablejsapi=1: The original 3k elo shockwave by ForellenLord himself comboing with his team's Rumble ultimate. ..Credit to XynergyLoL,
    https://www.youtube.com/embed/kLRa30aYYb8?autoplay=0&showinfo=0&rel=0&enablejsapi=1: Faker's version of the 3k elo shockwave against their rival team KT Rolster. Also known as the 4k elo shockwave. ..Credit to OPLOLReplay,
}
nextPage:
    url: /cptjack
    name: Cpt Jack Cleanse
theme: 0
type: player
---

It was game 5 in an intense series between Giants Gaming and Team Alternate. *Carl "ForellenLord" Lückmann* manages to execute one of the most memorable turnaround teamfight plays in professional history.

The *3k elo Shockwave* is a tribute to *Carl "ForellenLord" Lückmann* for his outstanding achievement of being the first player to ever reach an elo of 3000 in solo queue. This legacy celebrates successfully landing *Orianna's Shockwave* on multiple enemy champions thus making a devastating impact towards winning the teamfight. The *3k elo Shockwave* is known to be one of the best teamfight turnarounds and perfectly displays why *Orianna* has been a strong pick through all game metas.
---
title: Eggnivia Teleport
subtitle: The Froggen Special
player: Henrik "Froggen" Hansen
playerInfo: Henrik Hansen started his professional career in 2011 and has since become known for his innovative plays and unconventional champion picks. Some of his achievements include a record for reaching 300 cs the fastest in competitive play, popularizing Lee Sin in the mid lane, and reinventing the playstyle of Anivia. In 2016, he moved from EU to start as a mid laner for Echo Fox in NA.
year: 1234
quote:
quoteAuthor:
videos: {
    https://www.youtube.com/embed/O2TJzWCO2xQ?autoplay=0&showinfo=0&rel=0&enablejsapi=1: An example of Froggen himself performing the Eggnivia Teleport to escape. ..Credit to JK HighLight,
    https://www.youtube.com/embed/OSPbYswLb_I?autoplay=0&showinfo=0&rel=0&enablejsapi=1: Froggen pulling out the Eggnivia Teleport at Allstars Paris in 2014. ..Credit to OPLOLReplay,
    }
nextPage:
    url: /empire
    name: Empire
theme: 0
type: player
---

*Henrik "Froggen" Hansen* is known to be the best *Anivia* player in the world and is constantly inventing new playstyles for the champion. The *Eggnivia Teleport* relies on a special mechanic that only the champion *Anivia* has. Her passive morphs her into *Eggnivia* when she dies and after a few seconds, she is resurrected. Luckily, the morphing process does not interrupt any previously channelled ability thus ultimately increasing the total channel time window.

The *Eggnivia Teleport* is done when *Teleport* starts to channel in her bird form and is continued when she morphs into an egg. The key to successfully executing this technique is knowing that the egg form will survive any attacks until the *Teleport* channel finishes. This is a very unconventional play but may be useful when trying to escape when there are no other options!
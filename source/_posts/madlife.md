---
title: Madlife
subtitle: The Hook from the Future
player: Hong "MadLife" Min-gi
playerInfo: Hong Min-gi was previously an avid Starcraft player but transitioned to League of Legends in 2010. He has always focused on the Korean professional scene playing for teams such as CJ Entus and Azubu Frost. His professional career evolved in 2016 when he decided to play in the NACS for Gold Coin United.
year: 1234
quote: How in the world do you get to be that good. Madlife is God.
quoteAuthor: DoA and MonteCristo, Former OGN Casters
videos: {
    https://www.youtube.com/embed/BR6aR5du2QY?autoplay=0&showinfo=0&rel=0&enablejsapi=1: A montage featuring variations of The Madlife predicting different dodge movements. ..Credit to Fizz Khalifa,
    https://www.youtube.com/embed/uzth3cMJMRs?autoplay=0&showinfo=0&rel=0&enablejsapi=1: A detailed review on The Madlife with information on its variations and execution. ..Credit to TheRiverSaint,
    }
nextPage:
    url: /misaya
    name: Misaya
theme: 0
type: player
---

*Hong "MadLife" Min-gi* received the best support award in 2012 from Korean E-sports Awards for his exceptional and innovative plays on support champions. *The Madlife* is named after him for his consistency in landing calculated skill shots by predicting enemy dodge movements. What makes this so impressive is that these dodge movements are usually aided by skills such as *Flash* or *Ezreal's Arcane Shift* which make them very hard to hit.

This move is usually performed on hook champions such as *Blitzcrank* and *Thresh* but other skill shots that predict dodges can also be referred to as *The Madlife*. This technique relies on a huge amount of game and player knowledge to successfully execute and is reverred as one of the coolest plays in the game. 
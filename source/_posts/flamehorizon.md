---
title: Flame Horizon
subtitle: The Mark of Dominance
player: Lee "Flame" Ho-Jong
playerInfo: Lee Ho-Jong started his professional career in 2012 playing for the Korean team Azubu Blaze. He has awarded the Best Top Laner at 2013 Korean Esports Awards due to his ability to hard carry his team to victory. In 2016, he decided to pursue a career in North America playing for Immortals as their top laner.
year: 1234
quote:
quoteAuthor:
videos: {
    https://www.youtube.com/embed/Gl8P_tDND74?autoplay=0&showinfo=0&rel=0&enablejsapi=1: A 21 minute Flame Horizon achieved by IMT Flame in the NA LCS against Cloud 9. ..Credit to digested oddshot,
}
nextPage:
    url: /insec
    name: Insec
theme: 0
type: player
---

In older seasons of League of Legends, the meta game focused on a long laning phase until a mid to late game team fight decided the results of the match. During these seasons, players held their laning mechanics and abilities in paramount as these were the skills necessary to put your team ahead in the match. 

The *Flame Horizon* is named after *Lee "Flame" Ho-Jong* for his consistent domination in the top lane during laning oriented metas. Famously coined by previous OGN casters, *DoA* and *Montecristo*, *Flame Horizon* is used to describe situations where a player dominates his lane opponent outfarming them by at least *100cs*. 

In recent seasons, there is more focus on roaming and skirmishes across different lanes thus challenging the integrity of the *Flame Horizon*. However, the term is still used today to express a player's mark of dominance in the game and their *100cs* advantage. 
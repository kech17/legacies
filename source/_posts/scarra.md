---
title: Scarra Ward
subtitle: The Risky Click
player: William "scarra" Li
playerInfo: William Li is an American professional player in the NALCS. He is regarded as a veteran player playing for multiple NA teams as a jungler and mid laner. In 2014 he transitioned into a coach role for CLG but pivoted back to a professional player later in 2015. He is known as a highly mechanical Katarina player and has a large streaming fanbase.
year: 1234
quote: I hate this game. I hate this game so much.
quoteAuthor: William "scarra" Li
videos: {
    https://www.youtube.com/embed/MCisMmgkcgk?autoplay=0&showinfo=0&rel=0&enablejsapi=1: The infamous moment where scarra attempts to place the bottom tribrush ward from the dragon pit and fails. ..Credit to Communist Puppy.,
    https://www.youtube.com/embed/6pHQXNsBs58?autoplay=0&showinfo=0&rel=0&enablejsapi=1: Saintivicous and his solo queue team attemps to place the scarra ward and also fails. ..Credit to nightslut3.,
    https://www.youtube.com/embed/VUwexkc2xYA?autoplay=0&showinfo=0&rel=0&enablejsapi=1: Small tutorial on how to place the scarra ward on Summoner's Rift with the updated map textures. ..Credit to atrain472.,
    }
nextPage:
    url: /silverscrapes
    name: Silver Scrapes
theme: 0
type: player
---

The bottom *tribrush* is a highly contested vision objective and can be warded from the dragon pit to ensure safety and convenience. However, this technique is notorious for being very hard to execute as it has a low tolerance for positional error.

*William "scarra" Li* was the first professional player to popularize this technique and is known for failing at it multiple times before successfully mastering it. The secret to placing the *Scarra Ward* is to use object cues near the *tribrush*. However over the years, the layout of *Summoner's Rift* has undergone updates thus changing the locations of those object cues making this process inconsistent and confusing. 

*William "scarra" Li* will always be remembered for his innovative mechanical plays on *Team Dignitas* and his invention of the *Scarra Ward*.
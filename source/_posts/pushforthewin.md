---
title: Push For The Win
subtitle: The Underdog Story
player: GSG Team
playerInfo: GSG is a Korean Team that was formerly known as RoMg. They started in 2012 and was unfortunately disbanded in 2013. The team housed two future World Champions, EasyHoon and PoohManDu, that played for SKT T1. 
year: 1234
quote: GSG they've done it! They are the winter champions with Jungle Heimerdinger!
quoteAuthor: Julian "Pastrytime" Carr, Riot Caster
videos: {
    https://www.youtube.com/embed/nRAoZJolrYI?autoplay=0&showinfo=0&rel=0&enablejsapi=1: GSG vs CJ Entus in NLB Winter Finals Game 5 casted by Papasmithy and Pastrytime. ..Credit to eslasiatv,
}
nextPage: 
    url: /scarra
    name: Scarra Ward
theme: 0
type: player
---

CJ Entus was one of the top teams in the Korean region housing many of the best players including *inSec* and *dade*. Backed by a Korean conglomerate, CJ Group, they were the favorites the win the competition.

On the other hand, GSG was a complete amateur team. They had no sponsors and had to make time to practice together in internet cafes or PC bangs. They were the complete opposite of CJ Entus.

GSG fought tooth and nail to get to Game 5 of NLB Winter Finals. They decided risk it all and picked an unconventional push composition that included *Heimerdinger Jungle*. CJ Entus was thrown into confusion and 19 minutes, GSG brought down the third *inhibitor* and proceeded to become Winter Champions.

This was the ultimate underdog story and lead some players to grow into World Class professional players such as *Easyhoon (Easy)* and *PoohManDu (ManDu)*. 
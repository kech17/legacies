---
title: Silver Scrapes
subtitle: The Esports Anthem
player: Danny McCarthy
playerInfo: Danny McCarthy is a music composer that focuses on EDM but has since branched out to different genres including metal and rock. He was introduced to the community by LoL Esports as the composer for the famous song Silver Scrapes and has since continued to evolve his music career. You can find his presence on SoundCloud and his Facebook Page.
year: 1234
quote: Queue the Silver Scrapes! We're going to GAME 5!
quoteAuthor: James "Dash" Patterson, Riot Game's Analyst Desk Host
videos: {
    https://www.youtube.com/embed/AiGnL3XKctA?autoplay=0&showinfo=0&rel=0&enablejsapi=1: The Lol Esports feature of the origins of Silver Scrapes with its composer Danny McCarthy. ..Credit to LoL Esports.,
    https://www.youtube.com/embed/erlTPDI3s6w?autoplay=0&showinfo=0&rel=0&enablejsapi=1: The extended version of Silver Scrapes made by a fan to lengthen its beauty. ..Credit to Adriano Tg.,
    https://www.youtube.com/embed/aAZp3uqIqQg?autoplay=0&showinfo=0&rel=0&enablejsapi=1: Mark "MarkZ" Zimmerman's famous rendition of Silver Scrapes on the analyst desk. ..Credit to LoLRaynal.,
    }
nextPage:
    url: /smitevicious
    name: Smitevicious
theme: 0
type: history
---
It was the Season 2 World Championships broadcasted live for the world to see. Riot Games was still in its infancy when it came to live productions and fortunately through technical errors, they introduced Silver Scrapes to the world audience. Whenever the production team faced difficulties, they would take large breaks to attempt to resolve the issues. When this happened, they would temporarily display a static stream to the online audience and have Silver Scrapes play in the background. However, these breaks lasted for hours and were very often. 

Silver Scrapes was replayed so much that the community began to really enjoy the music for its heavy dubstep drops and electronic dance vibes. Season 2 World Championships lead the frontiers of Esports expansion at the time and Silver Scrapes had evolved to be its anthem. As a tribute, whenever a Best of 5 series reaches the last game, Silver Scrapes is played to commemorate how far Esports has come and celebrate the hype for the final game!
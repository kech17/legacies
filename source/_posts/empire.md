---
title: Empire
subtitle: The Brush of Death
player: Edward "Edward" Abgaryan
playerInfo: Formerly known as GoSu Pepper, Edward won his first IEM title in IEM Season VI on Team Empire. He plays the Support role and is well known for being a member of the European team Moscow Five which later went on to play in the Season 2 World Championships. He is currently still an active professional player in the LCL.
year: 1234
quote: Wow! They were not expecting that. SK just got destroyed.
quoteAuthor: Leigh "Deman" Smith, eSports Gaming Commentator
videos: {
    https://www.youtube.com/embed/e89llah-u3s?autoplay=0&showinfo=0&rel=0&enablejsapi=1: Intel Extreme Masters Season VI Kiev where Team Empire executes the first ever Empire. ..Credit to h1ndu,
    https://www.youtube.com/embed/VDU0EkLDBs8?autoplay=0&showinfo=0&rel=0&enablejsapi=1: A montage of Empire attempts that provide insight on how to bait the enemy team towards the Nunu ultimate. ..Credit to LeaguePoster,
    }
nextPage:
    url: /faker
    name: Faker's Zed
theme: 0
type: player
---

*The Empire* is an iconic technique that popularized a specific playstyle for the champion *Nunu*. During a qualifying match between SK Gaming and Team Empire at IEM Season VI Kiev, *Edward "Edward" Abgaryan* was able to successfully pull off a devastating *Nunu* ultimate to turn around a team fight. 

*The Empire* is based on the mechanic where enemies do not have sight of the *Nunu* ultimate if it's channeled within a brush making it easier to land. It is also common for *The Empire* to be executed with the help from an ally to bait the enemy team into the area of effect. 

However, this technique is so commonly used that enemies will most likely realize they are being *Empired* if they're slowed beside a brush for no reason.
---
title: xPeke
subtitle: The Backdoor Entrance
player: Enrique "xPeke" Cedeño Martínez
playerInfo: Enrique "xPeke" Cedeño Martínez* is a World Championship winner and has been playing League of Legends professionally since Season 1. He transitioned to mid lane and continued to play for Fnatic in EULCS until the end of 2014 when he formed the team Origen.
year: 1234
quote: That was the best best best thing I have seen in the entire history of League of Legends.
quoteAuthor: Joe Miller, eSports Gaming Commentator
videos: {
    https://www.youtube.com/embed/XdN67NuhMW4?autoplay=0&showinfo=0&rel=0&enablejsapi=1: Intel Extreme Masters Season VII Katowice where Enrique "xPeke" Cedeño Martínez first executed the famous xPeke backdoor. Now whenever a backdoor is done in a game people say "xPeke" to commemorate this moment in League of Legends history.  ..Credit to ESL,
    }
nextPage:
    url: /3kelo
    name: 3k ELO Shockwave
theme: 0
type: player
---

IEM Global Challenge is an international tournament that brings together the best teams from around the world. In IEM Katowice Season VII circuit, the two European powerhouses SK Gaming and Fnatic squared off and resulted in the moment we now know as the clutchest play in League of Legends history, *The xPeke*. 

In the final minutes of the game, Fnatic pushes to destroy the *Nexus* but is overwhelmed by SK Gaming's defense. SK Gaming decides to go for the game ending push which seems like a sure victory until *xPeke* makes the decision to backdoor their *Nexus*.

With *Kassadin's* versatile *Rift Walk*, *xPeke* was able to masterfully jump around the *Nexus* avoiding enemy defense and weaving auto attacks to secure the victory.

*Enrique "xPeke" Cedeño Martínez* showed amazing focus and mechanics on the World Stage and solidified his legacy with this play. To many, this play is revered as the greatest moment in League of Legends history.